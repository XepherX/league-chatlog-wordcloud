league-chatlog-wordcloud
========================

This simple python script converts a folder filled with riot's chatlog .json files into a text file ready to be used on [wordclouds.com](https://www.wordclouds.com/).

requirements
=======
python > 3.0  
that's it, no dependency hell here

useage
====

`python chatlog-parser.py ./path/to/chatLogs`