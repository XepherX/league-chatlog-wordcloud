import json
import glob
import sys

chatLogDirectory = "chatLogs"
if len(sys.argv) > 1:
	chatLogDirectory = sys.argv[1]


print("scanning directory {}".format(chatLogDirectory))
globbedFiles = glob.glob("./"+chatLogDirectory+"/*.json")
print("found {} files, beginning parsing.".format(len(globbedFiles)))

globalDict = {}

for globbedFile in globbedFiles:
	with open(globbedFile, "r") as chatLogFile:
		jsonContent = json.loads(chatLogFile.read())
		for sentText in jsonContent["text"]:
			for sentWord in sentText["chat"].split(" "):
				dictKey = sentWord.lower()
				if dictKey in globalDict:
					globalDict[dictKey] += 1
				else:
					globalDict[dictKey] = 1

sorted(globalDict)
sumTotalWords = 0
with open("output.txt", "w") as outputFileTxt:
	for (k, v) in globalDict.items():
		outputFileTxt.write("%i\t%s\n" % (v, k))
		sumTotalWords += v

print("successfully parsed {} distinct words ({} overall) from {} files".format(
	len(globalDict), sumTotalWords, len(globbedFiles)))
